<?php

/**
 * Incoming SMS parser for Yo!
 */

/**
 * Yo! parser class.
 */
class SmsYoUgandaParser extends SmsParserBase {
  /**
   * Restrict access to IP addresses supplied by Yo! These can change.
   *
   * If using smsframework/bootstrap/sms_incoming.inc and not loading variables
   * set $conf['sms_yo_uganda_server_ip1'] and $conf['sms_yo_uganda_server_ip2']
   * in your settings.php file.
   */
  static function checkAccess($ip, $request) {
    // Check IP if restricted
    if (variable_get('sms_yo_uganda_incoming_restrict_ip', FALSE)) {
      $ip1 = variable_get('sms_yo_uganda_incoming_server_ip1', '');
      $ip2 = variable_get('sms_yo_uganda_incoming_server_ip2', '');

      if ( (! empty($ip1) && $ip1 != $ip)
        && (! empty($ip2) && $ip2 != $ip) ) {
        return array(
          'headers' => array('Status' => '403 Forbidden'),
          'body' => 'Access not allowed from IP address',
        );
      }
    }

    // Production option possibly still available.
    /*
    $user = variable_get('sms_yo_uganda_incoming_username', '');
    $pass = variable_get('sms_yo_uganda_incoming_password', '');

    if ($request['username'] != $user
      || $request['password'] != $pass) {
      return array(
        'headers' => array('Status' => '403 Forbidden'),
        'body' => 'Authentication failed',
      );
    }
    */

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function parseRequest() {
    $this->number = $this->request['from'];
    // The telco's send us real nast funky encoded message
    // this is code to fix that for web
    $this->message = $this->utf8RawUrlDecode(html_entity_decode($this->request['text'], ENT_QUOTES, 'UTF-8'));
     $options = array();
    if (array_key_exists('to', $this->request) && !empty($this->request['to'])) {
      $options['receiver'] = $this->request['to'];
    }
    if (array_key_exists('from', $this->request) && !empty($this->request['from'])) {
      $options['sender'] = $this->request['from'];
    }
    if (array_key_exists('text', $this->request) && !empty($this->request['text'])) {
      $options['text'] = $this->request['text'];
    }
    if (array_key_exists('encoding', $this->request) && !empty($this->request['encoding'])) {
      $options['encoding'] = $this->request['encoding'];
    }
    if (array_key_exists('provider', $this->request) && !empty($this->request['provider'])) {
      $options['provider'] = $this->request['provider'];
    }
    if (array_key_exists('username', $this->request) && !empty($this->request['username'])) {
      $options['username'] = $this->request['username'];
    }
    if (array_key_exists('password', $this->request) && !empty($this->request['password'])) {
      $options['password'] = $this->request['password'];
    }
    $this->options = $options;
  }

  /**
   * Decoder for particular UTF8 supplied by Yo!
   */
  private function utf8RawUrlDecode ($source) {
    $decodedStr = "";
    $pos = 0;
    $len = strlen ($source);
    while ($pos < $len) {
      $charAt = substr ($source, $pos, 1);
      if ($charAt == '%') {
        $pos++;
        $charAt = substr ($source, $pos, 1);
        if ($charAt == 'u') {
          // we got a unicode character
          $pos++;
          $unicodeHexVal = substr ($source, $pos, 4);
          $unicode = hexdec ($unicodeHexVal);
          $entity = "&#". $unicode . ';';
          $decodedStr .= utf8_encode ($entity);
          $pos += 4;
        }
        else {
          // we have an escaped ascii character
          $hexVal = substr ($source, $pos, 2);
          $decodedStr .= chr (hexdec ($hexVal));
          $pos += 2;
        }
      } else {
        $decodedStr .= $charAt;
        $pos++;
      }
    }

    // And this is for all you UTF-16 suckers!! Eat this:
    $decodedStr = preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S','',$decodedStr);
    $decodedStr = preg_replace('/[^\x00-\x7F]+/S','',$decodedStr);

    return $decodedStr;
  }
}
