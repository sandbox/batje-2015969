-------------------------------------------------------------------
Using SMS Yo! Uganda provider with SMS Framework incoming SMS Queue
-------------------------------------------------------------------

In addition to the changes to your settings.php file descripbed in 
smsframework/bootstrap/README.txt if you want to restrict access to incoming
SMS to Yo! and are not loading variables you will also need to set:-

  // Set to TRUE to enable IP restriction.
  $conf['sms_yo_uganda_incoming_restrict_ip'] = TRUE;
  // The IP addresses that Yo! have supplied you that they will make requests
  // from.
  $conf['sms_yo_uganda_incoming_server_ip1'] = '1.1.1.1';
  $conf['sms_yo_uganda_incoming_server_ip2'] = '2.2.2.2';

  // If using username and password. NB Check implementation.
  $conf['sms_yo_uganda_incoming_username'] = 'username';
  $conf['sms_yo_uganda_incoming_password'] = 'password';

