<?php 
/*
 * Yopayments - A platform that enables business recieve or make payments from their mobile money accounts.
 *
 *
 * Copyright (C) 2006-2011 Yo! Uganda Limited, www.yo.co.ug,
 * All Rights Reserved
 *
 * Unauthorized redistribution of this software in any form or on any
 * medium is strictly prohibited. This software is released under a
 * license agreement and may be used or copied only in accordance with
 * the terms thereof. It is against the law to copy the software on
 * any other medium, except as specifically provided in the license
 * agreement.  No part of this publication may be reproduced, stored
 * in a retrieval system, or transmitted in any form or by any means,
 * electronic, mechanical, photocopied, recorded or otherwise, without
 * the prior written permission of Yo! Uganda Limited.
 */
 
 
class YoPaymentAPI {

	/*#######################################################################################################################
	 #APIUsername - This is the API Username which, together with the
     *            API Password below, maps your API request to your Yo! Payments
     *            account. Obtain this parameter from the web interface. Note
     *            that if you do not have a Business Account, you cannot use the
     *            API.
     #APIPassword - This is the API Password which, together with the
     *            API Username above, maps your API request to your Yo! Payments
     *            account. Obtain this parameter from the web interface. Note
     *            that if you do not have a Business Account, you cannot use the
     *            API.
	 #URL: This is the url to which xml data should be submited. Yopayments provides you with two urls. 
		* That is to say https://paymentsapi1.yo.co.ug/ybs/task.php and https://paymentsapi2.yo.co.ug/ybs/task.php.
	#########################################################################################################################
	 */
	function YoPaymentAPI($api_username,$api_password,$url)
	{
		$this->api_username = $api_username; 
		$this->api_password = $api_password;
		$this->url = $url;
	}
	
	
	/*#######################################################################################################################
	 #The function followUpTransaction below enables you to check up on the transaction that was earlier initiated. 
	 #In the response from the request that was made, there was transaction Reference id that uniquely identifies a particular transaction
	 #You are required to pass arguments to this funcation as follow:
	 #Pass the TransactReference as the only argument to this function. You may obtain this reference from your database where it was 
	 #stored
	#########################################################################################################################
	 */
	
	public function followUpTransaction($transaction_reference)
	{
		$xml_format = '<?xml version="1.0" encoding="UTF-8"?>
					<AutoCreate> 
						<Request>
							<APIUsername>'.$this->api_username.'</APIUsername>
							<APIPassword>'.$this->api_password.'</APIPassword>
							<Method>actransactioncheckstatus</Method> 
							<TransactionReference>'.$transaction_reference.'</TransactionReference> 
						</Request> 
					</AutoCreate>';
		//Extracting the fields of returned XML string in to an object array. 
		try {
				$content = $this->request($this->url, $xml_format);
				$return_array = new SimpleXMLElement( $content );
			} catch (Exception $ex) {
				try {
					$content = $this->postRequest($this->url, $xml_format);
					$return_array = new SimpleXMLElement( $content );
				} catch (Exception $ex) {
					
				}
			}
	
		if ($content !== "Error"  ) {
			$return = array();
			$return["Status"] = (isset($return_array->Response[0]->Status[0]) ? $return_array->Response[0]->Status[0] : '');
			$return["StatusCode"] = (isset($return_array->Response[0]->StatusCode) ? $return_array->Response[0]->StatusCode[0]: '');
			$return["StatusMessage"] = (isset($return_array->Response[0]->StatusMessage) ? $return_array->Response[0]->StatusMessage : '');
			$return["ErrorMessage"] = (isset($return_array->Response[0]->ErrorMessage) ? $return_array->Response[0]->ErrorMessage : '');
			$return["TransactionStatus"] = (isset($return_array->Response[0]->TransactionStatus) ? 
			$return_array->Response[0]->TransactionStatus : '');
			$return["TransactionReference"] = (isset($return_array->Response[0]->TransactionReference) ? 
			$return_array->Response[0]->TransactionReference : '');
			return $return;
		} elseif ($content !== '') {
			
		} else {
			return $content;
		}
		//return $content;
	}
	
	
	
	/*#######################################################################################################################
	 #The function checkAccBalance below enables you to check for you account balance with Yo Payments acocunt
	#########################################################################################################################
	 */
	
	public function checkAccBalance()
	{
		$xml_format = '<?xml version="1.0" encoding="UTF-8"?>
					<AutoCreate> 
						<Request>
							<APIUsername>'.$this->api_username.'</APIUsername>
							<APIPassword>'.$this->api_password.'</APIPassword>
							<Method>acacctbalance</Method> 
						</Request> 
					</AutoCreate>';
		
			//Extracting the fields of returned XML string in to an object array. 
		try {
				$content = $this->request($this->url, $xml_format);
				$return_array = new SimpleXMLElement( $content );
			} catch (Exception $ex) {
				try {
					$content = $this->postRequest($this->url, $xml_format);
					$return_array = new SimpleXMLElement( $content );
				} catch (Exception $ex) {
					
				}
			}
		if ($return_array) {
				$return = array();
				$return["Status"] = (isset($return_array->Response[0]->Status[0]) ? 
				$return_array->Response[0]->Status[0] : '');
				$return["StatusCode"] = (isset($return_array->Response[0]->StatusCode) ? 
				$return_array->Response[0]->StatusCode[0]: '');
				$return["balance"] = (isset($return_array->Response[0]->Balance[0]->Currency[0]->Balance) ? 
				$return_array->Response[0]->Balance[0]->Currency[0]->Balance : '');
				$return["Currency"] = (isset($return_array->Response[0]->Balance[0]->Currency[0]->Code) ? 
				$return_array->Response[0]->Balance[0]->Currency[0]->Code : '');
				$return["StatusMessage"] = (isset($return_array->Response[0]->StatusMessage) ? 
				$return_array->Response[0]->StatusMessage : '');
				$return["ErrorMessage"] = (isset($return_array->Response[0]->ErrorMessage) ? 
				$return_array->Response[0]->ErrorMessage : '');
			
		return $return;
		}
	}
	
	/*####################################################################################################################################
	#This funciton below will handle the Depositing of funds.The user is required to pass:
	#First argument as the Amount: This is the amount to be withdrawn. Must be set to a value
								* greater than zero. Fractional amounts may not be supported
								* by certain mobile money providers.
	#Second argument as the Account: This is a numerical value representing the
								* account number of the mobile money account where you wish to
								* transfer the funds. This is typically the telephone number
								* of the mobile phone receiving the amount. Telephone numbers
								* MUST have the international code prepended, without the �+�
								* sign. An example of a mobile money account number which would
								* be valid for the MTN Uganda network is 256771234567.
								
	#Third argument as AccountProviderCode: Provide here the account provider code of
								 * the institution holding the account indicated in the account
								 * parameter. See section 8 of API documentation for a list of all supported account
								 * provider code as the AccountProviderCode. 
								 
	#Forth argument as Narrative: Textual narrative about the transaction. Enter here a
								 * sentence describing the transaction. Provide a maximum of 4096
								 * characters here. If you wish to provide more information,
								 * consider using the Extended parameter method with
								 * NarrativeFileBase6. This field is mandatory.
								 
	#Fifth argument as NarrativeFileName: This parameter enables you to attach a file to
								 * the transaction. This is useful, for example, in the case
								 * where you may want to attach a scanned receipt, or a scanned
								 * payment authorization, depending on your internally
								 * established business rules. This parameter requires you to
								 * provide the name of the file you are attaching, as a string,
								 * for example �receipt.doc� or �receipt.pdf�. Note that the
								 * contents of this parameter are ignored if you have not
								 * provided the contents of the file using NarrativeFileBase64
								 * below.
								 
	#Sixth argument as NarrativeFileBase64: This parameter enables you to attach a file to the transaction.
								 * This is useful, for example, in the case where you may want to attached a scanned receipt,
								 * or a scanned payment authorization, depending on your business rules. 
								 * This parameter requires you to provide the contents of the file you are attaching, 
								 * encoded in base-64 encoding. Note that the contents of this parameter are ignored if
								 * you have not provided a file name using NarrativeFileName. This field is optional.
								 
	#Seventh argument as InternalReference: In this field, provide an internal transaction
								 * reference. If this transfer is related to another system
								 * transaction, enter its reference code in this field. If you
								 * are unsure about the meaning of this field, do not include it
								 * in your request. This field is useful in linking this request
								 * to another existing transaction which is already in the
								 * system.This fields is optional.
								 
	#Eighth argument as InternalReference:In this field, enter an external transaction
								 * reference. An external transaction reference is something
								 * which yourself and the benecifiary agree upon. For example,
								 * this may be an invoice number, or a phrase describing the
								 * purpose of this transaction in a way that the beneficiary
								 * would understand. This field is optional and you may omit it
								 * in your request.
	#Ninth argument as ProviderReferenceText:In this field, enter text you wish to be present 
								 * in any confirmation message which the mobile money provider 
								 * network sends to the subscriber upon successful completion 
								 * of the transaction. Some mobile money providers automatically
								 * send a confirmatory text message to the subscriber upon completion 
								 * of transactions. This parameter allows you to provide some text 
								 * which will be appended to any such confirmatory message sent to 
								 * the subscriber..							 
	#####################################################################################################################################
	*/
	public function depositFunds()
	{
		$arg = func_get_args();
		$xml_format = '<?xml version="1.0" encoding="UTF-8"?><AutoCreate><Request>
						<APIUsername>'.$this->api_username.'</APIUsername>
						<APIPassword>'.$this->api_password.'</APIPassword>';
		$xml_format .='<NonBlocking>TRUE</NonBlocking>';
		$xml_format .='<Method>acdepositfunds</Method>';
		$xml_format .=(isset($arg[0]) ? '<Amount>'.$arg[0].'</Amount>': '');
		$xml_format .=(isset($arg[1]) ? '<Account>'.$arg[1].'</Account>': '');
		$xml_format .=(isset($arg[2]) ? '<AccountProviderCode>'.$arg[2].'</AccountProviderCode>': '');
		$xml_format .=(isset($arg[3]) ? '<Narrative>'.$arg[3].'</Narrative>': '');
		$xml_format .=(isset($arg[4]) ? '<NarrativeFileName>'.$arg[4].'</NarrativeFileName>': '');
		$xml_format .=(isset($arg[5]) ? '<NarrativeFileBase64>'.$arg[5].'</NarrativeFileBase64>': '');
		$xml_format .=(isset($arg[6]) ? '<InternalReference>'.$arg[6].'</InternalReference>': '');
		$xml_format .=(isset($arg[7]) ? '<ExternalReference>'.$arg[7].'</ExternalReference>': '');
		$xml_format .=(isset($arg[8]) ? '<ProviderReferenceText>'.$arg[8].'</ProviderReferenceText>': '');
		$xml_format.='</Request></AutoCreate>';

		//Extracting the fields of returned XML string in to an object array. 
		try {
			$content = $this->request($this->url, $xml_format);
			$return_array = new SimpleXMLElement( $content );
		} catch (Exception $ex) {
			
			try {
				$content = $this->postRequest($this->url, $xml_format);
				$return_array = new SimpleXMLElement( $content );
			} catch (Exception $ex) {
				
			}
	
		}
		if ($content !== "Error"  ) {
			//Extracting the fields of returned XML string in to an object array. 
			//$return_array = new SimpleXMLElement( $content );
			$return = array();
			$return["Status"] = (isset($return_array->Response[0]->Status[0]) ? $return_array->Response[0]->Status[0] : '');
			$return["StatusCode"] = (isset($return_array->Response[0]->StatusCode) ? $return_array->Response[0]->StatusCode[0]: '');
			$return["StatusMessage"] = (isset($return_array->Response[0]->StatusMessage) ? $return_array->Response[0]->StatusMessage : '');
			$return["ErrorMessage"] = (isset($return_array->Response[0]->ErrorMessage) ? $return_array->Response[0]->ErrorMessage : '');
			$return["TransactionStatus"] = (isset($return_array->Response[0]->TransactionStatus) ? 
			$return_array->Response[0]->TransactionStatus : '');
			$return["TransactionReference"] = (isset($return_array->Response[0]->TransactionReference) ? 
			$return_array->Response[0]->TransactionReference : '');
			return $return;
		} elseif ($content !== '') {
			
		} else {
			return $content;
		}
		//return $content;
	}
	
	/*####################################################################################################################################
	#This funciton below handles withdraw of funds.The user is required to pass:
	#First argument as the Amount: This is the amount to be withdrawn. Must be set to a value
								* greater than zero. Fractional amounts may not be supported
								* by certain mobile money providers.
	#Second arguemnt as the Account: This is a numerical value representing the
								* account number of the mobile money account where you wish to
								* transfer the funds. This is typically the telephone number
								* of the mobile phone receiving the amount. Telephone numbers
								* MUST have the international code prepended, without the �+�
								* sign. An example of a mobile money account number which would
								* be valid for the MTN Uganda network is 256771234567. This fiels is optional.
								
	#Third argument as AccountProviderCode: Provide here the account provider code of
								 * the institution holding the account indicated in the Account
								 * parameter. See section 8 of API documentation for a list of all supported account
								 * provider code as the AccountProviderCode. 
								 
	#Forth argument as Narrative: Textual narrative about the transaction. Enter here a
								 * sentence describing the transaction. Provide a maximum of 4096
								 * characters here. If you wish to provide more information,
								 * consider using the Extended parameter method with
								 * NarrativeFileBase6. This field is mandatory.
								 
	#Fifth argument as NarrativeFileName: This parameter enables you to attach a file to
								 * the transaction. This is useful, for example, in the case
								 * where you may want to attach a scanned receipt, or a scanned
								 * payment authorization, depending on your internally
								 * established business rules. This parameter requires you to
								 * provide the name of the file you are attaching, as a string,
								 * for example �receipt.doc� or �receipt.pdf�. Note that the
								 * contents of this parameter are ignored if you have not
								 * provided the contents of the file using NarrativeFileBase64
								 * below.
								 
	#Sixth argument as NarrativeFileBase64: This parameter enables you to attach a file to the transaction.
								 * This is useful, for example, in the case where you may want to attached a scanned receipt,
								 * or a scanned payment authorization, depending on your business rules. 
								 * This parameter requires you to provide the contents of the file you are attaching, 
								 * encoded in base-64 encoding. Note that the contents of this parameter are ignored if
								 * you have not provided a file name using NarrativeFileName. This field is optional.
								 
	#Seventh argument as InternalReference: In this field, provide an internal transaction
								 * reference. If this transfer is related to another system
								 * transaction, enter its reference code in this field. If you
								 * are unsure about the meaning of this field, do not include it
								 * in your request. This field is useful in linking this request
								 * to another existing transaction which is already in the
								 * system.This fields is optional.
								 
	#Eighth argument as InternalReference:In this field, enter an external transaction
								 * reference. An external transaction reference is something
								 * which yourself and the benecifiary agree upon. For example,
								 * this may be an invoice number, or a phrase describing the
								 * purpose of this transaction in a way that the beneficiary
								 * would understand. This field is optional and you may omit it
								 * in your request.
								 
	#####################################################################################################################################
	*/
	public function withdrawFunds()
	{
		$arg = func_get_args();
		$xml_format = '<?xml version="1.0" encoding="UTF-8"?><AutoCreate><Request>
						<APIUsername>'.$this->api_username.'</APIUsername>
						<APIPassword>'.$this->api_password.'</APIPassword>';
		$xml_format .='<Method>acwithdrawfunds</Method>';
		$xml_format .=(isset($arg[0]) ? '<Amount>'.$arg[0].'</Amount>': '');
		$xml_format .=(isset($arg[1]) ? '<Account>'.$arg[1].'</Account>': '');
		$xml_format .=(isset($arg[2]) ? '<AccountProviderCode>'.$arg[2].'</AccountProviderCode>': '');
		$xml_format .=(isset($arg[3]) ? '<Narrative>'.$arg[3].'</Narrative>': '');
		$xml_format .=(isset($arg[4]) ? '<NarrativeFileName>'.$arg[4].'</NarrativeFileName>': '');
		$xml_format .=(isset($arg[5]) ? '<NarrativeFileBase64>'.$arg[5].'</NarrativeFileBase64>': '');
		$xml_format .=(isset($arg[6]) ? '<InternalReference>'.$arg[6].'</InternalReference>': '');
		$xml_format .=(isset($arg[7]) ? '<ExternalReference>'.$arg[7].'</ExternalReference>': '');
		$xml_format.='</Request></AutoCreate>';
		//Extracting the fields of returned XML string in to an object array. 
		try {
			$content = $this->request($this->url, $xml_format);
			$return_array = new SimpleXMLElement( $content );
		} catch (Exception $ex) {
			try {
				$content = $this->postRequest($this->url, $xml_format);
				$return_array = new SimpleXMLElement( $content );
			} catch (Exception $ex) {
				
			}
		}
		
		if ($content !== "Error" ) {
		//Extracting the fields of returned XML string in to an object array. 
			//$return_array = new SimpleXMLElement( $content );
			$return = array();
			$return["Status"] = (isset($return_array->Response[0]->Status[0]) ? $return_array->Response[0]->Status[0] : '');
			$return["StatusCode"] = (isset($return_array->Response[0]->StatusCode) ? $return_array->Response[0]->StatusCode[0]: '');
			$return["StatusMessage"] = (isset($return_array->Response[0]->StatusMessage) ? $return_array->Response[0]->StatusMessage : '');
			$return["ErrorMessage"] = (isset($return_array->Response[0]->ErrorMessage) ? $return_array->Response[0]->ErrorMessage : '');
			$return["TransactionStatus"] = (isset($return_array->Response[0]->TransactionStatus) ? 
			$return_array->Response[0]->TransactionStatus : '');
			$return["TransactionReference"] = (isset($return_array->Response[0]->TransactionReference) ? 
			$return_array->Response[0]->TransactionReference : '');
			return $return;
		} else {
			// Return status "error" if the request was unsuccessful.
			return $content;
		}
	}
	
	/*####################################################################################################################################
	#This funciton below will handle the Internal transfer of funds from one Yopayment business account to another.The user is required to pass:
	#First argument as the Amount: This is the amount to be withdrawn. Must be set to a value
								* greater than zero. Fractional amounts may not be supported
								* by certain mobile money providers.
	#Second arguemnt as the BeneficiaryAccount: This is a numerical value representing the
								* account number of the mobile money account where you wish to
								* transfer the funds. This is typically the telephone number
								* of the mobile phone receiving the amount. Telephone numbers
								* MUST have the international code prepended, without the �+�
								* sign. An example of a mobile money account number which would
								* be valid for the MTN Uganda network is 256771234567. This fiels is optional.
								
	#Third argument as CurrencyCode: Specify here the standard code of the currency in which you wish to perform the transfer.
								 * Note that (a) you must have a positive balance in the currency you specify;
								 * and (b) you must specify a valid currency code. Examples of valid currency 
								 * codes are UGX for �Uganda Shillings�, KES for �Kenyan Shillings�, USD for United. 
								 
	#Forth argument as BeneficiaryEmail: Provide here the email address of the recipient of the funds. 
								* You must provide a valid email address for the transaction to succeed. 
								* The Yo! Payments transaction processor will attempt to match the values you 
								* provide in the BeneficiaryAccount and BeneficiaryEmail with the values stored in the database. 
								* If they do not match, the transaction will not succeed.
								
	#Fifth argument as Narrative: Textual narrative about the transaction. Enter here a
								 * sentence describing the transaction. Provide a maximum of 4096
								 * characters here. If you wish to provide more information,
								 * consider using the Extended parameter method with
								 * NarrativeFileBase6. This field is mandatory.	
								 
	#Sixth argument as NarrativeFileName: This parameter enables you to attach a file to
								 * the transaction. This is useful, for example, in the case
								 * where you may want to attach a scanned receipt, or a scanned
								 * payment authorization, depending on your internally
								 * established business rules. This parameter requires you to
								 * provide the name of the file you are attaching, as a string,
								 * for example �receipt.doc� or �receipt.pdf�. Note that the
								 * contents of this parameter are ignored if you have not
								 * provided the contents of the file using NarrativeFileBase64
								 * below.
								 
	#Seventh argument as NarrativeFileBase64: This parameter enables you to attach a file to the transaction.
								 * This is useful, for example, in the case where you may want to attached a scanned receipt,
								 * or a scanned payment authorization, depending on your business rules. 
								 * This parameter requires you to provide the contents of the file you are attaching, 
								 * encoded in base-64 encoding. Note that the contents of this parameter are ignored if
								 * you have not provided a file name using NarrativeFileName. This field is optional.
								 
	#Eighth argument as InternalReference: In this field, provide an internal transaction
								 * reference. If this transfer is related to another system
								 * transaction, enter its reference code in this field. If you
								 * are unsure about the meaning of this field, do not include it
								 * in your request. This field is useful in linking this request
								 * to another existing transaction which is already in the
								 * system.This fields is optional.
								 
	#Ninth argument as InternalReference:In this field, enter an external transaction
								 * reference. An external transaction reference is something
								 * which yourself and the benecifiary agree upon. For example,
								 * this may be an invoice number, or a phrase describing the
								 * purpose of this transaction in a way that the beneficiary
								 * would understand. This field is optional and you may omit it
								 * in your request.
								 
	#####################################################################################################################################
	*/
	public function internalTransfer()
	{
		//Extract all arguments passed in to an array.
		$arg = func_get_args();
		// Create an XML string for internale transfer.
		$xml_format = '<?xml version="1.0" encoding="UTF-8"?><AutoCreate><Request>
						<APIUsername>'.$this->api_username.'</APIUsername>
						<APIPassword>'.$this->api_password.'</APIPassword>';
		$xml_format .='<Method>acinternaltransfer</Method>';
		$xml_format .=(isset($arg[0]) ? '<Amount>'.$arg[0].'</Amount>': '');
		$xml_format .=(isset($arg[1]) ? '<BeneficiaryAccount>'.$arg[1].'</BeneficiaryAccount>': '');
		$xml_format .=(isset($arg[2]) ? '<CurrencyCode>'.$arg[2].'</CurrencyCode>': '');
		$xml_format .=(isset($arg[3]) ? '<BeneficiaryEmail>'.$arg[3].'</BeneficiaryEmail>': '');
		$xml_format .=(isset($arg[4]) ? '<Narrative>'.$arg[4].'</Narrative>': '');
		$xml_format .=(isset($arg[5]) ? '<NarrativeFileName>'.$arg[5].'</NarrativeFileName>': '');
		$xml_format .=(isset($arg[6]) ? '<NarrativeFileBase64>'.$arg[6].'</NarrativeFileBase64>': '');
		$xml_format .=(isset($arg[7]) ? '<InternalReference>'.$arg[7].'</InternalReference>': '');
		$xml_format .=(isset($arg[8])?'<ExternalReference>'.$arg[8].'</ExternalReference>': '');
		$xml_format.='</Request></AutoCreate>';
		
		//Initiate an HTTP request to the server.
		//Extracting the fields of returned XML string in to an object array. 
		try {
			$content = $this->request($this->url, $xml_format);
			$return_array = new SimpleXMLElement( $content );
		} catch (Exception $ex) {
			try {
				$content = $this->postRequest($this->url, $xml_format);
				$return_array = new SimpleXMLElement( $content );
			} catch (Exception $ex) {
				
			}
		}
		if ($content !== "Error") {
		//Extracting the fields of returned XML string in to an object array. 
			//$return_array = new SimpleXMLElement( $content );
			$return = array();
			$return["Status"] = (isset($return_array->Response[0]->Status[0]) ? $return_array->Response[0]->Status[0] : '');
			$return["StatusCode"] = (isset($return_array->Response[0]->StatusCode) ? $return_array->Response[0]->StatusCode[0]: '');
			$return["StatusMessage"] = (isset($return_array->Response[0]->StatusMessage) ? $return_array->Response[0]->StatusMessage : '');
			$return["ErrorMessage"] = (isset($return_array->Response[0]->ErrorMessage) ? $return_array->Response[0]->ErrorMessage : '');
			$return["TransactionStatus"] = (isset($return_array->Response[0]->TransactionStatus) ? 
			$return_array->Response[0]->TransactionStatus : '');
			$return["TransactionReference"] = (isset($return_array->Response[0]->TransactionReference) ? 
			$return_array->Response[0]->TransactionReference : '');
			
			return $return;
		} else {
			// Return status "error" if the request was unsuccessful.
			return $content;
		}
	}
	
	/*
	###################################################################################################################################
	#This function makes internal HTTP request to the server. 
	# It takes two main arguments: 
	#1) Url - This the url where the Request is made. 
	#2) Data - The real content of data you are sending to the server. 
	# It returns the content from the server when the request is successfuly and and string "Error" when the request has failed.
	#Note: please make sure the cURL extenstion is enabled in your server otherwise the requests will fail.
	###################################################################################################################################
	*/
	
	private function request($url,$data)
	{
		set_time_limit(180);
		$headers = array('Content-type: text/xml\nContent-length: '.strlen($data).'\n Content-transfer-encoding: text\n\n'
		);
		try {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt($ch, CURLOPT_POST,           1 );
			curl_setopt($ch, CURLOPT_POSTFIELDS,     $data ); 
			curl_setopt($ch, CURLOPT_HTTPHEADER,     $headers ); 
			$response = curl_exec ($ch);
			return $response;
		} catch (Exception $ex) {
			return "Error";
		}
	}
	
	private function request2($url,$data)
	{
		set_time_limit(180);
		try {
			$r = new HttpRequest($url, HttpRequest::METH_POST);
			$headers = array('Content-type'=> 'text/xml',
			'Content-length'=>strlen($data),
			'Content-transfer-encoding'=>'text'
			);
			$r->setHeaders($headers);
			$r->setRawPostData($data);
			return $r->send()->getBody();
		} catch (HttpException $ex) {
			return "Error";
		}
	}
	
	/*
	###################################################################################################################################
	#This function makes internal HTTP request to the server. 
	# It takes two main arguments: 
	#1) Url - This the url where the Request is made. 
	#2) Data - The real content of data you are sending to the server. 
	# It returns the headers and return content from the server.
	###################################################################################################################################
	*/
	private function postRequest($url, $data, $referer='') {
	 
		// Convert the data array into URL Parameters like a=b&foo=bar etc.
		$data = $data;
		// parse the given URL
		$url = parse_url($url);
	 
		// extract host and path:
		$host = $url['host'];
		$path = $url['path'];
		// open a socket connection on port 80 - timeout: 30 sec
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
	 
		if ($fp){
			// send the request headers:
			fputs($fp, "POST $path HTTP/1.1\r\n");
			fputs($fp, "Host: $host\r\n");
	 
			if ($referer != '')
				fputs($fp, "Referer: $referer\r\n");
				fputs($fp, "Content-type: text/xml\r\n");
				fputs($fp, "Content-length: ". strlen($data) ."\r\n");
				fputs($fp, "Content-transfer-encoding: text\r\n");
				fputs($fp, "Connection: close\r\n\r\n");
				fputs($fp, $data);
		 
				$result = ''; 
				while(!feof($fp)) {
					// receive the results of the request
					$result .= fgets($fp, 128);
				}
			} else { 
				return array(
				'status' => 'err', 
				'error' => "$errstr ($errno)"
				);
			}
			// close the socket connection:
			fclose($fp);
			// split the result header from the content
			$result = explode("\r\n\r\n", $result, 2);
			$header = isset($result[0]) ? $result[0] : '';
			$content = isset($result[1]) ? $result[1] : '';
			// return as structured array:
			/*array(
				'status' => 'ok',
				'header' => $header,
				'content' => $content
			);*/
			return $content; 
	}
	
}

?>
